# WTF Testing otp login

## Usage

- Choose your otp reception method, type your email/phone number and click 'Get OTP'
- Once you recieve your otp, pass it into the otp field and click verify
- Now, you will be logged in.
- Click 'Goto your dashboard' to see your profile.
  If you are not logged in, you will be redirected back

## Debugging
- For testing, the entire database and non-expired otps are displayed in the `/data` route

## Deploying

- rename `.env.sample` to `.env`
- fill in the entries given
- run using `npm run start` or use `pm2 server.js` if you are using pm2