const sgMail = require('@sendgrid/mail');
const moment = require('moment');

// TODO: Validate env vars
const {
  SG_API_KEY,
  SG_SENDER,
  TWILIO_SID,
  TWILIO_TOKEN,
  TWILIO_SENDER,
  TWILIO_VERIFIED,
} = process.env;
const twilio = require('twilio');

sgMail.setApiKey(SG_API_KEY);
const basemail = {
  from: SG_SENDER,
  subject: 'Your otp for wtf signin',
};

function sendEmail(email, otp) {
  const mail = {
    ...basemail,
    to: email,
    text: `Your otp is ${otp}`,
  };
  sgMail.send(mail);
}

const twclient = twilio(TWILIO_SID, TWILIO_TOKEN);
async function sendPhone(phone, otp) {
  return twclient.messages
    .create({
      body: `Your otp is ${otp}`,
      from: TWILIO_SENDER,
      // for now only verified phones are supported
      // to: phone,
      to: TWILIO_VERIFIED,
    });
}


function checkExpiry(user) {
  return moment().isBefore(user.expiry);
}

function checkLogin(req, res, next) {
  if (!req.session.userId) {
    res.redirect('/?error="Not logged in"');
  } else {
    next();
  }
}

module.exports = {
  sendEmail,
  sendPhone,
  checkExpiry,
  checkLogin,
};
