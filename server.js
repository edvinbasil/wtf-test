require('dotenv').config();
const express = require('express');
const session = require('express-session');
const generate = require('nanoid/generate');
const low = require('lowdb');
const moment = require('moment');

const FileSync = require('lowdb/adapters/FileSync');

// Setup db
const adapter = new FileSync('db.json');
const db = low(adapter);
db.defaults({ auth: [] })
  .write();

const {
  sendEmail,
  sendPhone,
  checkExpiry,
  checkLogin,
} = require('./lib');

const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

// Get config vars
const {
  PORT = 8000,
  WTF_SESSION_SECRET,
} = process.env;
const app = express();
// Express middlewares
app.use(express.static('public'));
app.use(express.json());

// TODO: use a better session store maybe
app.use(session({
  name: 'wtf_sess',
  resave: false,
  saveUninitialized: false,
  secret: WTF_SESSION_SECRET,
  cookie: {
    sameSite: true,
    secure: false, // True in production with ssl
    maxAge: 3600 * 1000, // 1hr as defalt
  },
}));


app.use((req, res, next) => {
  const { userId } = req.session;
  if (userId) {
    const users = db.get('auth').filter(checkExpiry).value();
    users.forEach((user) => {
      if (user.otp === userId) {
        res.locals.user = user;
      }
    });
  }
  next();
});
app.post('/sendotp', async (req, res) => {
  const { user } = req.body;
  const otp = generate(alphabet, 5);
  const expiry = moment().add(2, 'minutes').toISOString();
  db.get('auth')
    .push({ user, otp, expiry })
    .write();
  if (req.body.type === 'email') {
    sendEmail(user, otp); // TODO: Validate user
  } else {
    await sendPhone(user, otp);
  }
  res.end();
});

app.get('/data', (req, res) => {
  const data = db.get('auth');
  res.json({
    valid: db.get('auth').filter(checkExpiry),
    data,
    now: moment(),
  });
});

app.post('/verify', (req, res) => {
  let auth = false;
  const { otp, user } = req.body;
  if (!otp) return res.status(400).end();
  const users = db.get('auth').filter({ user }).filter(checkExpiry).value();
  users.forEach((u) => {
    if (u.otp === otp) auth = true;
  });
  if (auth) {
    req.session.userId = otp;
    return res.json({ msg: 'success' });
  }
  return res.status(401).json({ msg: 'unauthorised' });
});


app.get('/dashboard', checkLogin, (req, res) => {
  const { user } = res.locals;
  res.send(`Hello user ${user.user}`);
});

app.listen(PORT, () => {
  console.log('Listening on port', PORT);
});
